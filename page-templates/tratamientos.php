<?php
/*
Template Name: Servicios
*/
get_header(); ?>

<?php get_template_part('template-parts/featured-image'); ?>
<div id="single-post" role="main">
<?php /* Start loop */ ?>
<?php while (have_posts()) : the_post(); ?>
<div id="buddypress">
<div id="members-treat" class="members">
		<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php the_content(); ?>
			<?php edit_post_link(__('Edit', 'foundationpress'), '<span class="edit-link">', '</span>'); ?>
			<div class="rouge">
			<?php
			get_tratamientos();
      ?>
			<div class="filter" data-filter="all"><span>Mostrar Todos</span></div>
		</div>
		<div class="row medium-unstack">

			<?php
			do_action('bp_before_members_loop'); ?>

			<?php if (bp_get_current_member_type()) : ?>
				<p class="current-member-type"><?php bp_current_member_type_message() ?></p>
			<?php endif;
			if (bp_has_members(bp_ajax_querystring('members'))) :
				do_action('bp_directory_members_content'); ?>
				<?php do_action('bp_before_directory_members_list'); ?>
				<?php while (bp_members()) : bp_the_member();

				//Limitando display a profesionales
				$user_id = bp_get_member_user_id();
				$member_type = bp_get_member_type($user_id);
				if ($member_type != 'profesional') {
					continue;
				}
				//clases de tratamientos para mixer
				$classes = bp_get_member_profile_data('field=Tratamientos');
				$classes = str_replace(', ', '.', $classes);
				$classes = str_replace(' ', '-', $classes);
				$classes = str_replace('.', ' ', $classes);

				?>


				<div class="member-prof mix <?php echo "$classes"; ?>" role="main">
					<div class="item-content">

						<div class="item-avatar">
							<a href="<?php bp_member_permalink(); ?>"><?php bp_member_avatar('type=full'); ?></a>
						</div>
						<div class="item">
							<div class="item-title">
								<h4><a href="<?php bp_member_permalink(); ?>"><?php bp_member_name(); ?></a></h4>
								<h5><?php bp_member_profile_data('field=Soy'); ?></h5>
								<span class="lead">Especialista en</span>
								<h4><?php bp_member_profile_data('field=Especialidad'); ?></h4>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>

<?php else : ?>

   <div id="message" class="info">
      <p><?php _e('Sorry, no members were found.', 'buddypress'); ?></p>
   </div>
<?php endif; ?>
</div>
</article>
	</div><!-- #members-dir-list -->
</div><!--Buddypress-->
	<?php endwhile; ?>
	<?php do_action( 'foundationpress_after_content' ); ?>
	<?php get_sidebar(); ?>
	<script src="//cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script type="text/javascript">
	jQuery(function ($){
	$('#members-treat').mixItUp();
	});
	</script>
<?php get_footer();

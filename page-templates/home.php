<?php
/*
Template Name: Home
*/
get_header(); ?>
<div class="promo" id="hero-home">

  <video id="video_bg" class="video-js" autoplay loop preload="auto"
    poster="<?php echo get_template_directory_uri();?>/assets/media/video0.jpg"
     data-setup='{ "controls": false, "autoplay": true, "preload": "yes", playbackRate: "0.75" }'>
     <source src="<?php echo get_template_directory_uri();?>/assets/media/video0.mp4" type="video/mp4" />
     <source src="<?php echo get_template_directory_uri();?>/assets/media/video0.webm" type="video/webm" />
  </video>
  <div class="video-filter">

  </div>
  <div class="row align-center contento">
    <div class="columns small-12 message">
      <div class="row align-middle">
        <div class="small-12 medium-4 columns">
          <?php  the_custom_logo();?>
        </div>
        <div class="small-12 medium-6 columns ">
          <div class="">
            <h1 ><span id="typed"></span></h1>
          </div>
          <div class="hide" id="typed-strings">
            <p>Tratamientos</p>
            <p>Spas</p>
            <p>Imagen</p>
            <p>Clínicas Estéticas</p>
            <p>Faciales</p>
            <p>Corporales</p>
            <p>Belleza</p>
          </div>
          <h2>Al alcance de un click</h2>
        </div>
      </div>
    </div>
    <div class="small-12 medium-6 column text-center message ">
      <!-- <h1>Busca</h1> -->
      <div class="callout">

        <?php
        if ( shortcode_exists( 'wpdreams_ajaxsearchpro' ) ) {
          echo do_shortcode('[wpdreams_ajaxsearchpro id=2]');
        }else{
          get_search_form();
        }?>
        <a href="servicios/" class="button inline"><i class="fa fa-tag fa-big"></i> Por servicio</a>
        <a href="especialidades/" class="button inline"><i class="fa fa-heart fa-big"></i> Por especialidades</a>
      </div>

    </div>
    <div class="medium-6 columns message">
      <div class="callout">
        <?php dwqa_get_ask_question_link( true, false, 'dwqa-btn dwqa-btn-block dwqa-btn-success button expanded large secondary' ); ?>
      </div>
    </div>
  </div>
</div>
<!-- Menu -->
<div data-sticky-container class="width-full grandmenu">
  <div class="sticky" data-sticky data-top-anchor="hero-home:bottom" data-btm-anchor="footer:bottom" data-margin-top='0'>
    <header id="masthead" class="site-header" role="banner">
      <div class="title-bar" data-responsive-toggle="site-navigation">
        <button class="menu-icon" type="button" data-toggle="mobile-menu"></button>
        <div class="title-bar-title">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
        </div>
      </div>

      <nav id="site-navigation" class="main-navigation top-bar row expanded" role="navigation">
        <div class="top-bar-left row">
          <div class="columns shrink">

            <?php echo get_custom_logo();?>
          </div>
            <div class="columns shrink">
              <a href="" class="button stronk secondary">Unete al catálogo</a>
            </div>
        </div>
        <div class="top-bar-right">
          <?php foundationpress_top_bar_r(); ?>

          <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'topbar' ) : ?>
            <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
          <?php endif; ?>
        </div>
      </nav>
    </header>
  </div>
</div>
<!-- Termina Menu -->
<div class="message width-full">
  <div class="row benefits">
    <div class="columns small-12">
      <h3>Navega en nuestro catálogo de  profesionales, spas, clínicas y centros de belleza
Encuentro lo que estás buscando por:</h3>
      <!-- <h2>Encuéntralos por:</h2> -->
    </div>
      <div class="columns medium-4 small-12">
        <div class="semantic">
      		<div class="icons">
      		  <i class="fa fa-tag fa-5x fa-fw"></i>
      		</div>
      		<h3>Servicios</h3>
      		<p>Busca el tratamiento que necesitas y descubre todos los profesionales que lo realizan, encuentra un listado completo comenzando con los lugares que lo destacan como su tratamiento estrella en su carta de servicios</p>
      	</div>
      </div>
    <div class="columns medium-4 small-12">
    	<div class="semantic">
        <div class="icons">
          <i class="fa fa-map-marker fa-5x fa-fw"></i>
        </div>
    		<h3>Localización</h3>
    		<p>Explora por la ubicación, conoce el centro estético más cercano a ti o ingresa la ciudad en donde te gustaría tomarlo. Conoce las opciones más convenientes si te encuentras en el trabajo, tu hogar o ¡de vacaciones!</p>
    	</div>
    </div>
    <div class="columns medium-4 small-12">
    	<div class="semantic">
        <div class="icons">
          <i class="fa fa-comments-o fa-5x fa-fw"></i>
        </div>
    		<h3>Experiencias</h3>
    		<p>Entérate de cuáles son las clínicas mejor posicionadas por la calidad de sus servicios,  filtra por ranking, y lee cada uno de los comentarios de otros pacientes que ya asistieron a ese lugar, ¡Ve a la segura!</p>
    	</div>

    </div>
  </div>
</div>

<section id="home-dinamic" class="container">
  <?php if ( have_posts() ) : ?>
    <div id="home-questions" class="row width-alt" >
      <div class="section-header white columns small-12">

        <h1 class="">Preguntas</h1>
        <br>
        <div class="row width-alt align-center small-up-1 medium-up-3 large-up-4">
          <?php
          $args_respuesta = array(
            'post_type' => 'dwqa-question',
            'showposts' => '8',
          );
          $author_posts = new WP_Query( $args_respuesta );
          if ( $author_posts->have_posts() ) :
            while ( $author_posts->have_posts() ) : $author_posts->the_post();
            get_template_part( 'template-parts/preguntas-home', get_post_format() );
          endwhile;
          wp_reset_postdata();
          endif
          ?>
        </div>
      </div>
  </div>
  <div id="home-testimony" class="row width-alt" >
    <div class="section-header columns small-12">

      <h1 class="text-left float-left">Testimonios</h1>
      <br>
    </div>
    <div class="columns small-12">
      <div class="row expanded align-spaced">
      <?php $testimonios = new WP_Query( array( 'post_type' => 'testimonio', 'showposts' => '8' ) );
      while ( $testimonios->have_posts() ) : $testimonios->the_post(); ?>

  <?php get_template_part( 'template-parts/content-front', get_post_format() ); ?>
         <?php endwhile; ?>
       </div>
    </div>
  </div>

  <div class="row width-alt">
    <div id="home-blog" class="columns small-12 medium-12" >
      <div class="section-header columns small-12">

        <h1>Diario</h1>
        <br>
      </div>

      <div class="front-news">

        <?php $latest = new WP_Query('showposts=8');
         while ( $latest->have_posts() ) : $latest->the_post();
         get_template_part( 'template-parts/content-front-2', get_post_format() );
         endwhile; ?>
          </div>
    </div>
    <?php else :
      get_template_part( 'template-parts/content', 'none' );
      endif; ?>
</div>
<?php do_action( 'foundationpress_after_content' ); ?>

  </section>

  <section id="home-call">
    <div class="row width-alt">
      <?php dynamic_sidebar( 'home-call' ); ?>

    </div>
  </section>

  <section id="home-fqa">
    <div class="row width-alt">
        <?php dynamic_sidebar( 'home-fqa' ); ?>

    </div>
  </section>
    <?php get_footer();

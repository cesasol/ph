<?php
$currentuser = get_userdata( bp_displayed_user_id() );
$currentuserid = $currentuser->ID;
// print_r($currentuser);
// echo "$currentuserid";

$member_type = bp_get_member_type( $currentuserid );
// echo "$member_type";
if ($member_type == 'profesional' ) :
// wp-get-template( 'members/single/index-nicename-profesional' );
include(TEMPLATEPATH . '/buddypress/members/single/index-nicename-profesional.php');
else :
  // bp_get_template_part( 'members/single/index' );
  get_header( 'buddypress' );?>
  <section class="container">
		<div id="page-full-width" role="main">
      <?php do_action( 'foundationpress_before_content' ); ?>
      <?php while ( have_posts() ) : the_post(); ?>
        <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
<?php
  include(TEMPLATEPATH . '/buddypress/members/single/home.php');
endwhile;?>
</article>
<?php do_action( 'foundationpress_after_content' );
get_footer( 'buddypress' );
endif;
 ?>

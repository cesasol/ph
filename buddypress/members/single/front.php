<?php
/**
 * BuddyPress - Members Front Page
 *
 * @since My Child Theme 2.6.0
 */
?>
<?php if ( is_user_logged_in() ) : ?>

  <?php bp_get_template_part( 'activity/post-form' ); ?>

<?php endif; ?>
<?php if ( bp_has_activities( bp_ajax_querystring( 'activity' ).'&scope=mentions' ) ) : ?>
  <?php if ( empty( $_POST['page'] ) ) : ?>

    <ul id="activity-stream" class="activity-list item-list">

  <?php endif; ?>

  <?php while ( bp_activities() ) : bp_the_activity(); ?>

    <?php bp_get_template_part( 'activity/entry' ); ?>

  <?php endwhile; ?>

  <?php if ( bp_activity_has_more_items() ) : ?>

    <li class="load-more">
      <a href="<?php bp_activity_load_more_link() ?>"><?php _e( 'Load More', 'buddypress' ); ?></a>
    </li>

  <?php endif; ?>

  <?php if ( empty( $_POST['page'] ) ) : ?>

    </ul>

  <?php endif; ?>

  <?php else : ?>

  <div id="message" class="info">
    <p><?php _e( 'Sorry, there was no activity found. Please try a different filter.', 'buddypress' ); ?></p>
  </div>

  <?php endif; ?>

  <?php

  /**
  * Fires after the finish of the activity loop.
  *
  * @since 1.2.0
  */
  do_action( 'bp_after_activity_loop' ); ?>

  <?php if ( empty( $_POST['page'] ) ) : ?>

  <form action="" name="activity-loop-form" id="activity-loop-form" method="post">

    <?php wp_nonce_field( 'activity_filter', '_wpnonce_activity_filter' ); ?>

  </form>

  <?php endif; ?>

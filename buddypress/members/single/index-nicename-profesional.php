<?php
/**
 * BuddyPress - Members Index Spa
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>
  <?php
/*Get HEader*/
get_header(); ?>
<!-- Get user info -->
<?php $ud = get_userdata( bp_displayed_user_id() ); ?>
<?php do_action( 'foundationpress_before_content' ); ?>

    <!-- Start buddy profile -->
    <div id="buddypress" class="spa_profile">
    <?php
    /*Get header for user*/
    do_action( 'bp_before_member_home_content' ); ?>
    <div id="item-header" role="complementary">
    <?php
		/**
		 * If the cover image feature is enabled, use a specific header
		 */
		if ( bp_displayed_user_use_cover_image_header() ) :
		bp_get_template_part( 'members/single/cover-image-header-spas' );
		else :
			bp_get_template_part( 'members/single/member-header' );
		endif;
		?>
    </div>
    <!-- #item-header -->
    <div class="spa_name">
      <?php if ( $ud->display_name ) : ?>
        <h1><?php echo $ud->display_name; ?></h1>
      <?php endif; ?>
      <br />
      <!-- Icons social -->
      <div class="social">
        <?php
        do_action( 'bp_before_member_header_meta' ); ?>
      </div>
    </div>
    <br>

    <br>
    <div id="spa_container">
      <div class="spa_body">
        <div class="spa_left">

          <?php
            $spa_type = bp_get_member_profile_data( array( 'field' => 'Soy') );// use bp_get_members_user_id() or just remove that argument if you are inside the members loop(e.g. on members directory).
            $spa_bio = xprofile_get_field_data( 'Bio');
            $spa_adress = xprofile_get_field_data( 'Dirección');
            $spa_phone = xprofile_get_field_data( 'Teléfono');
            $spa_cel = xprofile_get_field_data( 'Celular');
            $spa_mail = xprofile_get_field_data( 'Correo electrónico');
            $spa_best = xprofile_get_field_data( 'Especialidad');
            $spa_treat = xprofile_get_field_data( 'Tratamientos');
            $spa_user = get_userdata( bp_displayed_user_id() );
            $spa_username = $spa_user->user_login;

          ?>
          <h1>
            <?php if ( $spa_type ) { ?>
            <?php echo $spa_type; ?>
          <?php } ?>
          </h1>
          <h1 class="subheader">
            <?php if ( $spa_best ) { ?>

            Especialista en <?php echo $spa_best; ?>
          <?php } ?>
          </h1>
          <p>
            <?php if ( $spa_bio ) { ?>
            <i class="fa fa-quote-left fa-3x fa-pull-left fa-border" aria-hidden="true"></i><?php echo $spa_bio; ?>
          <?php } ?>
          </p>
          <hr>
          <ul class="fa-ul spa_info">
            <?php if ( $spa_phone ) { ?>
            <li><i class="fa-li fa fa-phone fa-fw"></i><?php echo $spa_phone; ?></li> <?php } ?><?php if ( $spa_cel ) { ?>
            <li><i class="fa-li fa fa-mobile fa-fw"></i><?php echo $spa_cel; ?></li> <?php } ?> <?php if ( $spa_mail ) { ?>
            <li><i class="fa-li fa fa-envelope-o fa-fw"></i><?php echo $spa_mail; ?></li> <?php } ?> <?php if ( $spa_adress ) { ?>
            <li><i class="fa-li fa fa-map-o fa-fw"></i><?php echo $spa_adress; ?></li> <?php } ?>
          </ul>
          <hr>
          <div class="respuestas">
            <h1>Respuestas</h1>

            <?php
             // bp_get_template_part( 'members/single/activity-answer' );
             $args_respuesta = array(
                  'author_name' => $spa_username,
                  'post_type' => 'dwqa-answer',
              );
              $author_posts = new WP_Query( $args_respuesta );
              if ( $author_posts->have_posts() ) :
                  while ( $author_posts->have_posts() ) : $author_posts->the_post();
                  get_template_part( 'template-parts/respuestas', get_post_format() );
                  endwhile;
                  wp_reset_postdata();
              endif ?>
              <p>
                <?php
                dwqa_get_ask_question_link( true, false, 'dwqa-btn dwqa-btn-block dwqa-btn-success button expanded large secondary' );
                ?>

              </p>

          </div>
          <div class="columns ">
            <?php
             if ( $spa_adress ) {
               echo do_shortcode( '[location address="'.$spa_adress.'" is_responsive="yes"]' ); }
               ?>

          </div>

        </div>
        <div class="spa_right">
          <div class="title width-full">

            <h1>Servicios</h1>
          </div>
          <div id="tratamientos">
          <!-- PENDIENTE, hacer este resultado en etiquetas -->
          <div class="">

            <ul class="services">

              <?php

              if ($spa_treat ) {
                foreach ( $spa_treat as $spa_treats ) {
                  echo "<li>$spa_treats</li>";
                }
              }
              ?>
            </ul>
          </div>
        </div>
        <hr>
        <div class="testamentos">
          <h1>Testimonios</h1>
          <?php $testimonios = new WP_Query( array( 'experiencia' => "'.$spa_username.'" ) ); ?>
          <?php if ( $testimonios->have_posts() ) : ?>
          <?php while ( $testimonios->have_posts() ) : $testimonios->the_post(); ?>
            <?php get_template_part( 'template-parts/testimonios', get_post_format() ); ?>
          <?php endwhile; wp_reset_postdata(); ?>
        <?php else : ?>
          <p>
            Se el primero en contar tu experiencia en <?php if ( $ud->display_name ) : ?>
              <?php echo $ud->display_name; ?>
              <?php endif; ?>
          </p>
          <?php endif; // End have_posts() check.
          if ( is_user_logged_in() ) {
            ?>
            <p><a data-open="testo_modal" class="button expanded success">Cuenta tu historia</a></p>
            <div class="reveal" id="testo_modal" data-reveal>
              <?php echo do_shortcode('[cmb-frontend-form post_type="testimonio"]'); ?>
              <button class="close-button" data-close aria-label="Close reveal" type="button">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <?php
          }else {
            ?>
            <p><a data-toggle="login_modal_testo"  class="button expanded secondary">Inicia sesión para contar tu historia</a></p>
            <div class="small reveal" data-reveal data-close-on-click="true" data-animation-in="spin-in" data-animation-out="spin-out" id="login_modal_testo" data-reveal>
              <?php
              // Login form arguments.
              $args = array(
                  'remember'       => true,
                  'value_username' => NULL,
                  'value_remember' => true
              );
              // Calling the login form.
              wp_login_form( $args );
               ?>
              <button class="close-button" data-close aria-label="Close reveal" type="button">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php } ?>
        </div>
        <hr>
        </div>
      </div>
        <div class="comentario">
          <hr>
          <h2>Comentarios</h2>
          <div id="item-nav">
            <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
              <ul>

                <?php bp_get_displayed_user_nav(); ?>

                <?php

                /**
                 * Fires after the display of member options navigation.
                 *
                 * @since 1.2.4
                 */
                do_action( 'bp_member_options_nav' ); ?>

              </ul>
            </div>
          </div><!-- #item-nav -->
          <?php
          do_action( 'bp_before_member_body' );
          if ( bp_is_user_front() ) :
    	 			bp_displayed_user_front_template_part();
    		 elseif ( bp_is_user_activity() ) :
           bp_get_template_part( 'members/single/activity' );

         elseif ( bp_is_user_blogs() ) :
           bp_get_template_part( 'members/single/blogs'    );

         elseif ( bp_is_user_friends() ) :
           bp_get_template_part( 'members/single/friends'  );

         elseif ( bp_is_user_groups() ) :
           bp_get_template_part( 'members/single/groups'   );

         elseif ( bp_is_user_messages() ) :
           bp_get_template_part( 'members/single/messages' );

         elseif ( bp_is_user_profile() ) :
           bp_get_template_part( 'members/single/profile'  );

         elseif ( bp_is_user_forums() ) :
           bp_get_template_part( 'members/single/forums'   );

         elseif ( bp_is_user_notifications() ) :
           bp_get_template_part( 'members/single/notifications' );

         elseif ( bp_is_user_settings() ) :
           bp_get_template_part( 'members/single/settings' );

         // If nothing sticks, load a generic template
         else :
           bp_get_template_part( 'members/single/plugins'  );

         endif;

         /**
          * Fires after the display of member body content.
          *
          * @since 1.2.0
          */
         do_action( 'bp_after_member_body' );
           ?>
        </div>
      </div>
    </div>

<?php do_action( 'bp_after_member_body' ); ?>
    </div>
    <!-- End #BuddyPress -->
    <?php do_action( 'foundationpress_after_content' ); ?>

<?php get_footer();

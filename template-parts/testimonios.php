<?php
/**
 * Testimonios
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'respuestas-perfil callout'); ?>>

  <header>
		<h6><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
	</header>
	<div class="entry-content ">
		<?php the_content( __( 'Continue reading...', 'foundationpress' ) ); ?>
    <cite class="user-nicename">@<?php the_author(); ?></cite>
	</div>
	<hr />
</div>

<?php
/**
 * Testimonios
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<div class="question-container columns">

  <div id="post-<?php the_ID(); ?>" <?php post_class( 'blogpost-entry article-card '); ?>>

    <header class="columns">
      <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
    </header>
    <div class="columns">
      <p>
        Preguntado por  <?php if (bp_core_get_userlink( $post->post_author )) {
          echo bp_core_get_userlink( $post->post_author );
        }else{
          echo "Anónimo";
        } ?>
      </p>
    </div>
    <div class="columns">
      <i class="fa fa-question fa-3x fa-pull-left fa-rotate-180" aria-hidden="true"></i>
      <?php the_excerpt(); ?>

    </div>
    <hr />
  </div>
</div>

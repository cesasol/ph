<?php
/**
 * TPara las noticias del home
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
    <div id="post-<?php the_ID(); ?>" <?php post_class( 'blogpost-entry col-home-post article-card'); ?> >
      <?php if ( has_post_thumbnail() ) : ?>
        <div class="tumba ">
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php the_post_thumbnail(); ?>
          </a>
        </div>
        <?php else : ?>
          <div class="tumba">
          </div>
          <?php endif; ?>
          <div class="fondo">
            <header>
              <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
              <?php foundationpress_entry_meta(); ?>
            </header>

            <div class="entry-excerpt">
              <?php the_excerpt( __( 'Continue reading...', 'foundationpress' ) ); ?>
            </div>
            <footer>
              <?php $tag = get_the_tags(); if ( $tag ) { ?>
                <p>
                  <?php the_tags(); ?>
                </p>
                <?php } ?>
            </footer>
            <hr />
          </div>
    </div>

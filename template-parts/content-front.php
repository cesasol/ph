<?php
/**
 * Testimonios.
 *
 * Used for both single and index/archive/search.
 *
 * @since FoundationPress 1.0.0
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry home-experiencia article-card '); ?>>

  <?php     if (has_post_thumbnail($post->ID)) :
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
            $image = $image[0];
              ?>

    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
    <div class="tumba" style="background-image: url('<?php echo $image ?>')">
    </div>
    </a>
    <?php else : ?>

      <?php endif; ?>
        <div class="columns">

          <header>
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
          </header>
          <div class="entry-excerpt columns small-12">
            <?php the_excerpt(__('(more...)'));?>
            <cite class="user-nicename">@<?php the_author(); ?></cite>

              <!--More-->
          </div>
          <hr />
        </div>
</div>

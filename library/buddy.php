<?php

function wpseo_fix_title_buddypress($title) {
     // Check if we are in a buddypress page
     if (function_exists( 'buddypress') && buddypress()->displayed_user->id || buddypress()->current_component ) {
         $bp_title_parts = bp_modify_document_title_parts();

         // let's rebuild the title here
         $title = $bp_title_parts['title'] . ' ' . $title;
     }
     return $title;
 }
 add_filter( 'wpseo_title', 'wpseo_fix_title_buddypress');

function get_tratamientos(){
  if (is_page()) {

    global $wpdb;
    $tablename = $wpdb->prefix . "bp_xprofile_data";
    $field = xprofile_get_field_id_from_name('Tratamientos');
    $sql = "SELECT value FROM " . $tablename . " WHERE field_id = %d;";
    $prep_sql = $wpdb->prepare( $sql, $field );
    $results = $wpdb->get_row( $prep_sql );
    $result_treat = $results->value;
    preg_match_all("/(?:(?:\"(?:\\\\\"|[^\"])+\")|(?:'(?:\\\'|[^'])+'))/is", $result_treat, $array_treat);
    $nospaces = preg_replace('/[\s_]/', '-', $array_treat[0]);
    $uncoated = preg_replace('/"/A', '".', $nospaces);
    $string_treat = implode("\n", $array_treat[0]);
    $spa_treat = str_replace('"', '', $string_treat);
    // echo "<pre>";
    // print_r($results);
    // print_r($array_treat);
    // print_r(array_combine($uncoated, $array_treat[0]));
    // echo "$string_treat </pre> <br> <hr>";
    // echo "$spa_treat";
    // todos los tratamientos están listados en esta variable listos para añadirse a clase
    foreach (array_combine($uncoated, $array_treat[0]) as $spa_treats => $spa_treats2) {
      echo "<div class=\"filter\" data-filter=$spa_treats><span>$spa_treats2</span></div>";
    }
    unset($spa_treats);
    unset($spa_treats2);
  }
}
function get_expecialidad(){
  if (is_page()) {
    global $wpdb;
    $tablename = $wpdb->prefix . "bp_xprofile_data";
    $field = xprofile_get_field_id_from_name('Soy');
    $sql = "SELECT value FROM " . $tablename . " WHERE field_id = %d;";
    $prep_sql = $wpdb->prepare( $sql, $field );
    $results = $wpdb->get_col( $prep_sql );
    foreach ($results as $key => $value){
      $nospaces[$key] = strtolower(str_replace(' ', '_', $value));
    }
    unset($key);
    unset($value);
    $dots = preg_filter('/^/', '.', $nospaces);
    // var_dump($nospaces);
    // var_dump($dots);
    foreach (array_combine($dots, $results) as $spa_treats => $spa_treats2) {
        echo "<div class=\"filter\" data-filter=$spa_treats><span>$spa_treats2</span></div>";
    }
    unset($spa_treats);
    unset($spa_treats2);
  }
}

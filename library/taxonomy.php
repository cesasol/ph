<?php
// Register Custom Post Type
function testimonio_post_type() {

	$labels = array(
		'name'                  => _x( 'Testimonios', 'Post Type General Name', 'foundationpress' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'foundationpress' ),
		'menu_name'             => __( 'Testimonios', 'foundationpress' ),
		'name_admin_bar'        => __( 'Testimonio', 'foundationpress' ),
		'archives'              => __( 'Archivo de testimonios', 'foundationpress' ),
		'parent_item_colon'     => __( 'Parent Item:', 'foundationpress' ),
		'all_items'             => __( 'All Items', 'foundationpress' ),
		'add_new_item'          => __( 'Add New Item', 'foundationpress' ),
		'add_new'               => __( 'Add New', 'foundationpress' ),
		'new_item'              => __( 'New Item', 'foundationpress' ),
		'edit_item'             => __( 'Edit Item', 'foundationpress' ),
		'update_item'           => __( 'Update Item', 'foundationpress' ),
		'view_item'             => __( 'View Item', 'foundationpress' ),
		'search_items'          => __( 'Search Item', 'foundationpress' ),
		'not_found'             => __( 'Not found', 'foundationpress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'foundationpress' ),
		'featured_image'        => __( 'Featured Image', 'foundationpress' ),
		'set_featured_image'    => __( 'Set featured image', 'foundationpress' ),
		'remove_featured_image' => __( 'Remove featured image', 'foundationpress' ),
		'use_featured_image'    => __( 'Use as featured image', 'foundationpress' ),
		'insert_into_item'      => __( 'Insert into item', 'foundationpress' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'foundationpress' ),
		'items_list'            => __( 'Items list', 'foundationpress' ),
		'items_list_navigation' => __( 'Items list navigation', 'foundationpress' ),
		'filter_items_list'     => __( 'Filter items list', 'foundationpress' ),
	);
  $capabilities = array(
		'edit_post'             => 'edit_test',
		'read_post'             => 'read_test',
		'delete_post'           => 'delete_test',
		'edit_posts'            => 'edit_test',
		'edit_others_posts'     => 'edit_others_test',
		'publish_posts'         => 'publish_test',
		'read_private_posts'    => 'read_private_test',
	);
	$args = array(
		'label'                 => __( 'Testimonio', 'foundationpress' ),
		'description'           => __( 'Testimonios de los servicios provistos por profesionales.', 'foundationpress' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', ),
		'taxonomies'            => array( 'experiencia' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-status',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => $capabilities,
	);
	register_post_type( 'testimonio', $args );

}
add_action( 'init', 'testimonio_post_type', 0 );


// Register Custom Taxonomy
function experiencia_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Experiencias', 'Taxonomy General Name', 'foundationpress' ),
		'singular_name'              => _x( 'Experiencia', 'Taxonomy Singular Name', 'foundationpress' ),
		'menu_name'                  => __( 'Experiencias', 'foundationpress' ),
		'all_items'                  => __( 'All Items', 'foundationpress' ),
		'parent_item'                => __( 'Parent Item', 'foundationpress' ),
		'parent_item_colon'          => __( 'Parent Item:', 'foundationpress' ),
		'new_item_name'              => __( 'New Item Name', 'foundationpress' ),
		'add_new_item'               => __( 'Add New Item', 'foundationpress' ),
		'edit_item'                  => __( 'Edit Item', 'foundationpress' ),
		'update_item'                => __( 'Update Item', 'foundationpress' ),
		'view_item'                  => __( 'View Item', 'foundationpress' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'foundationpress' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'foundationpress' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'foundationpress' ),
		'popular_items'              => __( 'Popular Items', 'foundationpress' ),
		'search_items'               => __( 'Search Items', 'foundationpress' ),
		'not_found'                  => __( 'Not Found', 'foundationpress' ),
		'no_terms'                   => __( 'No items', 'foundationpress' ),
		'items_list'                 => __( 'Items list', 'foundationpress' ),
		'items_list_navigation'      => __( 'Items list navigation', 'foundationpress' ),
	);
	$rewrite = array(
		'slug'                       => 'experiencia',
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$capabilities = array(
		'manage_terms'               => 'manage_experiencias',
		'edit_terms'                 => 'manage_experiencias',
		'delete_terms'               => 'manage_experiencias',
		'assign_terms'               => 'edit_test',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
		'capabilities'               => $capabilities,
	);
	register_taxonomy( 'experiencia', array( 'testimonio' ), $args );

}
add_action( 'init', 'experiencia_taxonomy', 0 );
